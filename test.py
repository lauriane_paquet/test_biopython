#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from Bio.PDB import *

# var
protName = '1n55'
pdbName = protName + '.pdb'

# create structure object
parser = PDBParser(PERMISSIVE=1) # permissive; allow reading buggy pdb files
structure = parser.get_structure(protName, pdbName)

# access header information (see if able to catch 2ary struct)
headerKeywords = ["keywords", "name", "head", "deposition_date", "release_date", "structure_method", "resolution", "structure_reference", "journal_reference", "author", "compound"]
print(str(structure.header))
for keyword in headerKeywords :
    print(keyword + " : " + str(structure.header[keyword]))


# # select only x and write it
# class GlySelect(Select):
#     def accept_residue(self, residue):
#         if residue.get_name()=='GLY':
#             return 1
#         else:
#             return 0
#
# io = PDBIO()
# io.set_structure(s)
# io.save('gly_only.pdb', GlySelect())

# navigate through a Structure object
for model in structure:
    for chain in model:
        for residue in chain:
            for atom in residue:
                print(atom)

atoms = structure.get_atoms()
residues = structure.get_residues()
atoms = chain.get_atoms()

    # Get all residues from a structure
res_list = Selection.unfold_entities(structure, 'R')
    # Get all atoms from a chain
atom_list = Selection.unfold_entities(chain, 'A')
    # A=atom, R=residue, C=chain, M=model, S=structure

    # examples of extract a specific Atom/Residue/Chain/Model from a Structure?
model = structure[0]
chain = model['A']
residue = chain[100]
atom = residue['CA']
atom = structure[0]['A'][100]['CA']
print(atom)
    # usages ici model 0 -> prot; model x -> mvt prot

# information from an Atom
a = atom
a.get_name()       # atom name (spaces stripped, e.g. 'CA')
a.get_id()         # id (equals atom name)
a.get_coord()      # atomic coordinates
a.get_vector()     # atomic coordinates as Vector object
a.get_bfactor()    # isotropic B factor
a.get_occupancy()  # occupancy
a.get_altloc()     # alternative location specifier
a.get_sigatm()     # std. dev. of atomic parameters
a.get_siguij()     # std. dev. of anisotropic B factor
a.get_anisou()     # anisotropic B factor
a.get_fullname()   # atom name (with spaces, e.g. '.CA.')

# distances btw atoms : Simply subtract the atoms to get their distance
distance = structure[0]['A'][100]['CA'] - structure[0]['A'][101]['CA']
print(distance)

# can calc angles and angles

# add model to structure ?
model_id = 1
serial_num = None
model = Model(model_id, serial_num)
structure.add(model)

# writting pdb file
io = PDBIO()
io.set_structure(structure)
io.save('out.pdb')

